# yagri

generated using Luminus version "3.32"

FIXME

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

You need a configuration file for the dev environment. By default you can just rename the file example-dev-config.edn to dev-config.edn.


To start a web server for the application, run:
    
    docker-compose up -d
    lein repl
    (migrate)
    (start)

## License

Copyright © 2019 FIXME
