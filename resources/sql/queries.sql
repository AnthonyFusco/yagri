-- :name save-post! :! :n
-- :doc creates a new post
INSERT INTO post
    (title, message, timestamp)
VALUES (:title, :message, :timestamp)

-- :name get-posts :? :*
-- :doc selects all available posts (and associated like count)
select post.*, count(likes.iduser) as likes
from post
         left join likes on post.id = likes.idpost
group by id

-- :name post-by-id :? :1
-- :doc Get post (and associated like count) by id
select post.*, count(likes.iduser) as likes
from post
         left join likes on post.id = likes.idpost
where post.id = :id
group by id

-- :name like-post! :! :n
-- :doc Like a post
INSERT into likes (idpost, iduser)
values (:idpost, :iduser)
on conflict (idpost, iduser) do nothing;
