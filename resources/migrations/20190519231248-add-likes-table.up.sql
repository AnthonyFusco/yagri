CREATE TABLE likes
(
    iduser VARCHAR(200) NOT NULL,
    idpost SERIAL       NOT NULL REFERENCES post (id),
    primary key (iduser, idpost)
);