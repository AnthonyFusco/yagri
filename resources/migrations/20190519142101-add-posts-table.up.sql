CREATE TABLE post
(id SERIAL PRIMARY KEY,
 title VARCHAR(200),
 message TEXT,
 timestamp TIMESTAMP);