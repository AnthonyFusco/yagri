(ns yagri.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[yagri started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[yagri has shut down successfully]=-"))
   :middleware identity})
