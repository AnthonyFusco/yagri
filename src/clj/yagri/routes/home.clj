(ns yagri.routes.home
  (:require
    [yagri.layout :as layout]
    [yagri.db.core :as db]
    [clojure.java.io :as io]
    [yagri.middleware :as middleware]
    [ring.util.http-response :as response]
    [struct.core :as st]
    [schema.core :as s]
    [reitit.coercion.schema]
    [schema.core :as s]
    [clojure.tools.logging :as log]))

(def message-schema
  [[:title
    st/required
    st/string]

   [:message
    st/string]])

(defn validate-message [params]
  (first (st/validate params message-schema)))

(defn save-post! [{:keys [params]}]
  (if-let [errors (validate-message params)]
    (-> (response/found "/")
        (assoc :flash (assoc params :errors errors)))
    (do
      (db/save-post!
        (assoc params :timestamp (java.util.Date.)))
      (response/found "/"))))

(defn like-post! [{{:keys [id-post id-user]} :params}]
  (db/like-post! {:idpost (Integer/parseInt id-post) :iduser id-user}) ; could be better
  (response/found "/"))

(defn home-page [{:keys [flash] :as request}]
  (layout/render
    request
    "home.html"
    (merge
      {:id-user (:user (:session request))}
      {:messages (db/get-posts)}
      (select-keys flash [:id :title :errors]))))

(defn about-page [request]
  (layout/render request "about.html"))

(defn post-page [requests]
  (layout/render
    requests
    "post.html"
    {:post (db/post-by-id (:path (:parameters requests)))}))

(defn set-user! [id {session :session}]
  (-> (response/accepted (str "User set to: " id))
      (assoc :session (assoc session :user id))
      (assoc :headers {"Content-Type" "text/plain"})))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get  home-page
         :post save-post!}]
   ["/post/:id" {:get {:coercion   reitit.coercion.schema/coercion
                       :parameters {:path {:id s/Int}}
                       :handler    post-page}}]
   ["/like" {:post like-post!}]
   ["/login/:id" {:get (fn [{:keys [path-params] :as req}]
                         (set-user! (:id path-params) req))}]
   ["/about" {:get about-page}]])
