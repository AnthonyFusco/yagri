FROM openjdk:8-alpine

COPY target/uberjar/yagri.jar /yagri/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/yagri/app.jar"]
